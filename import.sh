ogr2ogr -f "MySQL" MySQL:electionquebec,user=root,password=root data/Circonscriptions/Circonscription_electorale_\(VG\).shp -nln circonscription -append -lco GEOMETRY_NAME=polygon

ogr2ogr -f "ESRI Shapefile" circonscriptions2008.shp MySQL:electionquebec,user=root,password=root -sql "SELECT * from circonscription"

ogr2ogr -f "MySQL" MySQL:electionquebec,user=root,password=root data/update/Circonscription_electorale_\(NC\).shp -nln updatecirconscriptions -append -lco GEOMETRY_NAME=polygon

ogr2ogr -f "ESRI Shapefile" circonscriptions2012.shp MySQL:electionquebec,user=root,password=root -sql "SELECT * from updatecirconscriptions"

echo "insert into circonscriptions_codepostal(codepostal, circonscription_id) values" | awk -F"," '{ print "("$1", select id from circonscriptions_circonscirption where co_cep = "$2"),"}' codepostaux.csv  > test

ogr2ogr -f "KML" fichier MySQL:electionquebec,user=root,password=root -sql "SELECT * from updatecirconscriptions where nmtri_cep = 'fichier'"