from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('representants.views',
    url(r'^(?P<annee>\d{4})/circonscriptions/(?P<nom>[a-z]+)/representants/?$', 'liste_representant', name='liste_representant'),
    url(r'^(?P<annee>\d{4})/circonscriptions/(?P<nom>[a-z]+)/resultat/?$', 'resultat_circonscription', name='resultat_circonscription'),
)