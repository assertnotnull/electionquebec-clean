from django.db import models
from circonscriptions.models import Circonscription

class Parti(models.Model):
    nom = models.CharField(max_length=100)
    site_web = models.CharField(max_length=100, null=True)

    def json(self):
        return {
            'nom' : self.nom,
            'site_web' : self.site_web
        }

    def __unicode__(self):
        return self.nom

class Representant(models.Model):
    nom = models.CharField(max_length=30)
    prenom = models.CharField(max_length = 30)
    photo = models.ImageField(upload_to='images/', null=True)

    def json(self):
        fields = ('nom', 'prenom', 'photo')
        return dict((field, self.__dict__[field]) for field in fields)

    def __unicode__(self):
        return self.nom + ', ' + self.prenom

class Resultat_Representant(models.Model):
    representant = models.ForeignKey(Representant)
    annee = models.IntegerField()
    pourcentage = models.FloatField(null=True)
    majorite = models.IntegerField(null=True)
    bulletins_valides = models.IntegerField()
    circonscription = models.ForeignKey(Circonscription)
    parti = models.ForeignKey(Parti)

    def json(self, fields):
        allfields = {
            'representant' : self.representant.json(),
            'parti' : self.parti.json(),
            'annee' : self.annee,
            'pourcentage' : self.pourcentage,
            'majorite' : self.majorite,
            'bulletins_valides' : self.bulletins_valides
        }
        dicts = (fields, allfields)
        return dict((field, allfields[field]) for field in fields)

    def __unicode__(self):
        return self.representant.name + self.annee


class PositionChefs(models.Model):
    representant = models.ForeignKey(Representant)
    date = models.DateTimeField()
    longitude = models.FloatField()
    latitude = models.FloatField()

    def __unicode__(self):
        return self.longitude + self.latitude

