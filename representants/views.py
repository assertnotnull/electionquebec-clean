import json
from django.http import HttpResponse
from circonscriptions.models import Circonscription, CodePostal
from common.jsonresponse import JsonpResponse, JSONResponse
from representants.models import Representant, Resultat_Representant

def resultat_circonscription(request, nom, annee):
    circonscription = Circonscription.objects.get(nmtri_cep = nom.upper(), annee=annee)
    fields = ('representant', 'parti', 'annee', 'pourcentage', 'majorite', 'bulletins_valides')
    data = [resultat.json(fields) for resultat in Resultat_Representant.objects.filter(circonscription = circonscription, annee = annee).select_related()]
    return JSONResponse(data)

def liste_representant(request, nom, annee):
    circonscription = Circonscription.objects.get(nmtri_cep = nom.upper(), annee=annee)
    fields = ('representant', 'parti', 'annee')
    data = [resultat.json(fields) for resultat in Resultat_Representant.objects.filter(circonscription = circonscription, annee = annee).select_related()]
    return JSONResponse(data)