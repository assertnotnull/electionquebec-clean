"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from django.contrib.gis.geos.point import Point
from django.core.urlresolvers import reverse

from django.test import TestCase
from django.test.client import Client
from circonscriptions.models import CodePostal, Circonscription
from representants.models import Representant, Parti, Resultat_Representant


class RechercheRepresentantParCodePostal(TestCase):
    def setUp(self):
        shape = Point(12.546,45.676)
        self.circonscription = Circonscription.objects.create(ogr_fid=1, id_cep=1, nm_cep='Montreal', nmtri_cep='MONTREAL',
            co_cep=222, shape=shape, simple_shape=shape, dh_maj='2012-01-01')
        self.codepostal = CodePostal.objects.create(circonscription=self.circonscription, codepostal='H8K1L4')
        self.parti = Parti.objects.create(nom='parti de rien')
        self.representant = Representant.objects.create(nom='ti', prenom='gus')
        self.resultat = Resultat_Representant.objects.create(
            representant = self.representant,
            annee = 2008,
            pourcentage = 23.5,
            circonscription = self.circonscription,
            bulletins_valides = 200,
            parti = self.parti,
        )
        self.client = Client()

    def testRecherche_Resultat_Avec_Succes(self):
        reponse = self.client.get(reverse('resultat_circonscription', kwargs={'nom' :'montreal', 'annee': '2008'}))
        self.assertContains(reponse, 'pourcentage')
        self.assertEqual(reponse.status_code, 200)

    def testRecherche_Representant_Avec_Succes(self):
        reponse = self.client.get(reverse('liste_representant', kwargs={'nom' :'montreal', 'annee': '2008'}))
        self.assertContains(reponse, 'gus')
        self.assertEqual(reponse.status_code, 200)