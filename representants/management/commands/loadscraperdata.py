# encoding: utf-8
from urllib2 import urlopen
import json
from django.core.management.base import BaseCommand
from circonscriptions.models import Circonscription
from representants.models import Representant, Parti, Resultat_Representant

class Command(BaseCommand):
    help = u'Obtient les données du scraper'

    def get_version(self):
        return '1'

    def load_data(self):
        pass

    def handle(self, *args, **options):
        urlparti = 'https://api.scraperwiki.com/api/1.0/datastore/sqlite?format=jsondict&name=election_quebec_2008' + \
                   '&query=select%20distinct%20%60parti%60%20from%20%60swdata%60'
        url = 'https://api.scraperwiki.com/api/1.0/datastore/sqlite?format=jsondict&name=election_quebec_2008' + \
              '&query=select%20*%20from%20%60swdata%60%20order%20by%20%60circonscription%60%20'
        data = urlopen(urlparti)
        Parti.objects.all().delete()
        for jsonparti in json.loads(data.read()):
            Parti.objects.get_or_create(
                nom = jsonparti['parti']
            )

        data = urlopen(url)
        Representant.objects.all().delete()
        Resultat_Representant.objects.all().delete()
        for jsonrepresentant in json.loads(data.read()):
            nom = jsonrepresentant['nom'].split(',')
            representant = Representant.objects.get_or_create(
                nom = nom[0],
                prenom = nom[1].strip(),
            )
            circonscription = jsonrepresentant['circonscription'].strip()
            print circonscription
            #les invalides: gaspé, maskinongé, bourassa-sauvé et tous ceux qui ont "?"
            representant[0].resultat_representant_set.create(
                annee = 2008,
                pourcentage = jsonrepresentant['pourcentage'].replace(',', '.'),
                bulletins_valides = jsonrepresentant['bulletins_valides'],
                majorite = jsonrepresentant['majorite'],
                parti = Parti.objects.get(nom = jsonrepresentant['parti']),
                circonscription = Circonscription.objects.get(nm_cep = circonscription, annee=2008),
            )

