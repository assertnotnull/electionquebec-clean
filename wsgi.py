import os
import sys
# put the Django project on sys.path
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../")))
sys.path.insert(0, '/var/local/python/electionquebec/')
sys.path.insert(0, '/var/local/python/env/electionquebec/lib/python2.6/site-packages')
os.environ["DJANGO_SETTINGS_MODULE"] = "electionquebec.settings.active"
from django.core.handlers.wsgi import WSGIHandler
application = WSGIHandler()