# -=- encoding: utf-8 -=-
import re
from appconf.base import AppConf
from django.core.validators import RegexValidator
from django.contrib.gis.db import models

r_postalcode = re.compile(r'^[GHJ]\d[ABCEGHJKLMNPRSTVWXYZ]\d[ABCEGHJKLMNPRSTVWXYZ]\d$')

class MyAppConf(AppConf):
    MAX_GEO_LIST_RESULTS = 350 # In a /boundary/shape query, if more than this
    # number of resources are matched, throw an error
    SIMPLE_SHAPE_TOLERANCE = 0.0002
    SHAPEFILES_DIR = './data/shapefiles'

app_settings = MyAppConf()

class Circonscription(models.Model):
    ogr_fid = models.IntegerField(max_length=11)
    # Identifiant unique et primaire d'un enregistrement d'une circonscription provinciale
    id_cep = models.DecimalField(max_digits=5,decimal_places=0)

    # Le code de circonscription électorale est un identifiant unique pour une circonscription qui provient de
    # l'Institut de la statistique du Québec.
    # Il peut servir de clé de recherche dans la sélection d'une circonscription.
    co_cep = models.DecimalField(max_digits=3,decimal_places=0)

    # Nom de la circonscription électorale telle que spécifié dans la Gazette officielle du Québec lors de
    # la publication de la liste des circonscriptions électorales faite par la Commission de
    # la représentation électorale du Québec.
    nm_cep = models.CharField(max_length=30)

    # Le nom de tri permet de trier les circonscriptions selon un ordre spécifique au DGE.
    # Il est formé en prenant le nom transformé en majuscules non accentuées, en enlevant
    # tous les caractères qui ne sont pas des lettres et en transformant
    # tous les "SAINT", "SAINTE", "ST" et "STE" en "SAINT".
    nmtri_cep = models.CharField(max_length=30, db_index=True)
    dh_maj = models.DateField()
    shape = models.GeometryField()
    simple_shape = models.GeometryField()
    centroid = models.PointField(null=True)
    objects = models.GeoManager()
    annee = models.IntegerField(db_index=True)

class CodePostal(models.Model):
    circonscription = models.ForeignKey(Circonscription)
    codepostal = models.CharField(max_length=6, validators=[RegexValidator(r_postalcode)], db_index=True)
