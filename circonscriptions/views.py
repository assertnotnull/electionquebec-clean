# -=- encoding: utf-8 -=-
import json
from django.contrib.gis.geos.geometry import GEOSGeometry
from django.contrib.gis.geos.point import Point
from django.http import  Http404, HttpResponse
from django.shortcuts import render_to_response
from common.jsonresponse import  JsonpResponse, JSONResponse
from circonscriptions.models import Circonscription, CodePostal

def recherche_code_postal(request,codepostal,annee):
    codepostalmodel = CodePostal.objects.select_related().get(codepostal = codepostal.upper())
    circonscription = Circonscription.objects.filter(annee = annee)
    circonscription = circonscription.filter(codepostal = codepostalmodel)
    return JSONResponse(circonscription.values('nm_cep', 'nmtri_cep', 'ogr_fid','simple_shape'))

def recherche_code_postal_approx(request, codepostal, annee):
    codepostalmodel = CodePostal.objects.select_related().filter(codepostal__startswith = codepostal.upper())[:1]
    circonscription = Circonscription.objects.filter(annee = annee)
    circonscription = circonscription.filter(codepostal = codepostalmodel)
    return JSONResponse(circonscription.values())

def recherche_circonscription(request, nom, annee):
    nomclean = nom.replace('ste','sainte').replace('st','saint').replace('-','').upper()
    circonscription = Circonscription.objects.filter(annee = annee)
    circonscription = circonscription.filter(nmtri_cep = nomclean)
    if len(circonscription.values()) is 0:
        logger.info(u"La circonscription {} n'as pas été trouvé".format(nomclean))
        raise Http404
    return JSONResponse(circonscription.values('nm_cep', 'nmtri_cep', 'ogr_fid','simple_shape'))

def recherche_lat_lng(request, lng, lat, annee):
    point = Point(float(lng), float(lat))
    circonscription = Circonscription.objects.filter(annee = annee)
    circonscription = circonscription.filter(shape__contains=point)
    return JSONResponse(circonscription.values('nm_cep', 'nmtri_cep', 'ogr_fid','simple_shape'))
