from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('circonscriptions.views',
    #url(r'^(?P<annee>\d{4})/codepostal/(?P<codepostal>[A-Za-z0-9]{6})/?$', 'recherche_code_postal', name='recherche_code_postal'),
    #url(r'^(?P<annee>\d{4})/codepostal/(?P<codepostal>\w\d\w)/?$', 'recherche_code_postal_approx', name='recherche_code_postal'),
    url(r'^(?P<annee>\d{4})/circonscriptions/(?P<nom>\w+)/?$', 'recherche_circonscription', name='recherche_circonscription'),
    url(r'^(?P<annee>\d{4})/location/(?P<lng>[\-0-9.]+)/(?P<lat>[\-0-9.]+)/?$', 'recherche_lat_lng', name='recherche_lat_lng'),
)