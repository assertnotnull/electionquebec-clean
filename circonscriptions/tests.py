"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from django.contrib.gis.geos.point import Point
from django.core.urlresolvers import reverse

from django.test import TestCase
from django.test.client import Client
from circonscriptions.models import CodePostal, Circonscription
from django.core.exceptions import ObjectDoesNotExist


class RechercheCodePostal(TestCase):
    def setUp(self):
        shape = Point(12.546,45.676)
        self.circonscription = Circonscription.objects.create(ogr_fid=1, id_cep=1, nm_cep='Montreal', nmtri_cep='MONTREAL',
            co_cep=222, shape=shape, simple_shape=shape, dh_maj='2012-01-01')
        self.codepostal = CodePostal.objects.create(circonscription=self.circonscription, codepostal='H8K1L4')
        self.client = Client()
    def testRechercheCodePostal_AvecSucces(self):
        response = self.client.get(reverse('recherche_code_postal', args=['h8k1l4']))
        self.assertContains(response,'MONTREAL')
        self.assertEqual(response.status_code, 200)
    def testRechercheCodePostal_Invalide(self):
        with self.assertRaises(ObjectDoesNotExist):
            response = self.client.get(reverse('recherche_code_postal', args=['l3l4l5']))
            self.assertEqual(response.status_code, 404)
    def testRechercheCodePostal_TropCourt(self):
        response = self.client.get('/codepostal/l4l4l')
        self.assertEqual(response.status_code, 404)
    def testRechercheCodePostal_TropLong(self):
        response = self.client.get('/codepostal/l4hl5ll5l66l')
        self.assertEqual(response.status_code, 404)
    def testRechercheCirconsription_AvecSucces(self):
        response = self.client.get(reverse('recherche_circonscription', args=['montreal']))
        self.assertContains(response, 'MONTREAL')
        self.assertEqual(response.status_code, 200)
    def testRechercheCirconscription_AvecEchec(self):
        response = self.client.get(reverse('recherche_circonscription', args=['inconnu']))
        self.assertEqual(response.status_code, 404)
