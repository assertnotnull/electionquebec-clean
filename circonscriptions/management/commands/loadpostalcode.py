from django.core.management.base import BaseCommand
from circonscriptions.models import CodePostal, Circonscription

class Command(BaseCommand):
    help = 'Load csv data of postal codes as: code,district_id'
    option_list = BaseCommand.option_list

    def get_version(self):
        return '1'

    def handle(self, *args, **options):
        file = open('data/codepostaux/codepostaux.csv', 'r')
        size = len(file.readlines())
        file.seek(0)
        i = 0.0
        CodePostal.objects.all().delete()
        oldval = None
        for value in file.readlines():
            values = value.split(',')
            if oldval == values[1].strip():
                cir.codepostal_set.create(codepostal = values[0])
            else:
                cir = Circonscription.objects.get(co_cep=values[1].strip())
                oldval = values[1].strip()
            cir.codepostal_set.create(codepostal = values[0])