#coding: utf8

import logging
from django.contrib.gis.gdal.field import OFTInteger
import circonscriptions

log = logging.getLogger(__name__)
from optparse import make_option
import os, os.path
import sys

from zipfile import ZipFile
from tempfile import mkdtemp

from django.conf import settings
from django.contrib.gis.gdal import CoordTransform, DataSource, OGRGeometry, OGRGeomType
from django.core.management.base import BaseCommand
from django.db import connections, DEFAULT_DB_ALIAS, transaction
from django.template.defaultfilters import slugify

from circonscriptions.models import app_settings, Circonscription

GEOMETRY_COLUMN = 'shape'

class Command(BaseCommand):
    help = 'Import boundaries described by shapefiles.'
    option_list = BaseCommand.option_list + (
        make_option('-r', '--reload', action='store_true', dest='reload',
            help='Reload BoundarySets that have already been imported.'),
        make_option('-u', '--database', action='store', dest='database',
            default=DEFAULT_DB_ALIAS, help='Specify a database to load shape data into.'),
        make_option('-d', '--delete', action='store_true', dest='delete', help='Delete before importing'),
        )

    def get_version(self):
        return '0.1'

    def handle(self, *args, **options):
        # Load configuration
        kind = app_settings.SHAPEFILES_DIR
        config = {'file' : kind}
        self.load_set(kind, config, options)

    @transaction.commit_on_success
    def load_set(self, kind, config, options):
        print ('Processing %s.' % kind)

        path = config['file']
        datasources = create_datasources(path)

        if options['delete'] is True:
            #Make sure it is empty
            print 'Deleting all circonscriptions'
            Circonscription.objects.all().delete()

        for datasource in datasources:
            log.info("Loading %s from %s" % (kind, datasource.name))
            # Assume only a single-layer in shapefile
            if datasource.layer_count > 1:
                log.warn('%s shapefile [%s] has multiple layers, using first.' % (datasource.name, kind))
            layer = datasource[0]
            self.add_boundaries_for_layer(config, layer, options['database'], datasource.name.split('/')[3][-8:-4], options)


    def polygon_to_multipolygon(self, geom):
        """
        Convert polygons to multipolygons so all features are homogenous in the database.
        """
        if geom.__class__.__name__ == 'Polygon':
            g = OGRGeometry(OGRGeomType('MultiPolygon'))
            g.add(geom)
            return g
        elif geom.__class__.__name__ == 'MultiPolygon':
            return geom
        else:
            raise ValueError('Geom is neither Polygon nor MultiPolygon.')

    def add_boundaries_for_layer(self, config, layer, database, year, options):
        # Get spatial reference system for the postgis geometry field
        geometry_field = Circonscription._meta.get_field_by_name(GEOMETRY_COLUMN)[0]
        SpatialRefSys = connections[database].ops.spatial_ref_sys()
        db_srs = SpatialRefSys.objects.using(database).get(srid=geometry_field.srid).srs

        if 'srid' in config and config['srid']:
            layer_srs = SpatialRefSys.objects.get(srid=config['srid']).srs
        else:
            layer_srs = layer.srs

        # Create a convertor to turn the source data into
        transformer = CoordTransform(layer_srs, db_srs)

        log.info('Inserting')
        for feature in layer:
            # Transform the geometry to the correct SRS
            geometry = self.polygon_to_multipolygon(feature.geom)
            geometry.transform(transformer)

            # Create simplified geometry field by collapsing points within 1/1000th of a degree.
            # Since Chicago is at approx. 42 degrees latitude this works out to an margin of
            # roughly 80 meters latitude and 112 meters longitude.
            # Preserve topology prevents a shape from ever crossing over itself.
            simple_geometry = geometry.geos.simplify(app_settings.SIMPLE_SHAPE_TOLERANCE, preserve_topology=True)

            # Conversion may force multipolygons back to being polygons
            simple_geometry = self.polygon_to_multipolygon(simple_geometry.ogr)

            feature = UnicodeFeature(feature, encoding=config.get('encoding', 'utf-8'))

            # Extract metadata into a dictionary
#            metadata = dict(
#                ( (field, feature.get(field)) for field in layer.fields )
#            )
#
#            external_id = str(config['id_func'](feature))
#            feature_name = config['name_func'](feature)
#            feature_slug = unicode(slugify(config['slug_func'](feature)).replace(u'—', '-'))

            centroid = str(geometry.geos.centroid)

            if options['delete'] is True:
                Circonscription.objects.create(
                    ogr_fid=feature.get('ogr_fid'),
                    id_cep=feature.get('id_cep'),
                    co_cep=feature.get('co_cep'),
                    nm_cep=feature.get('nm_cep'),
                    nmtri_cep=feature.get('nmtri_cep'),
                    dh_maj=feature.get('dh_maj'),
                    shape=geometry.wkt,
                    simple_shape=simple_geometry.wkt,
                    centroid=centroid,
                    annee=year)
            else:
                circonscription = Circonscription.objects.get(ogr_fid=feature.get('ogr_fid'))
                circonscription.id_cep=feature.get('id_cep')
                circonscription.co_cep=feature.get('co_cep')
                circonscription.nm_cep=feature.get('nm_cep')
                circonscription.nmtri_cep=feature.get('nmtri_cep')
                circonscription.dh_maj=feature.get('dh_maj')
                circonscription.shape=geometry.wkt
                circonscription.simple_shape=simple_geometry.wkt
                circonscription.centroid=centroid
                circonscription.annee=year
                circonscription.save()

def create_datasources(path):
    if path.endswith('.zip'):
        path = temp_shapefile_from_zip(path)

    if path.endswith('.shp'):
        return [DataSource(path)]

    # assume it's a directory...
    sources = []
    for fn in os.listdir(path):
        fn = os.path.join(path,fn)
        if fn.endswith('.zip'):
            fn = temp_shapefile_from_zip(fn)
        if fn.endswith('.shp'):
            sources.append(DataSource(fn))
    return sources

class UnicodeFeature(object):

    def __init__(self, feature, encoding='ascii'):
        self.feature = feature
        self.encoding = encoding

    def get(self, field):
        val = self.feature.get(field)
        if isinstance(val, str):
            return val.decode(self.encoding)
        return val

def temp_shapefile_from_zip(zip_path):
    """Given a path to a ZIP file, unpack it into a temp dir and return the path
       to the shapefile that was in there.  Doesn't clean up after itself unless
       there was an error.

       If you want to cleanup later, you can derive the temp dir from this path.
    """
    zf = ZipFile(zip_path)
    tempdir = mkdtemp()
    shape_path = None
    # Copy the zipped files to a temporary directory, preserving names.
    for name in zf.namelist():
        data = zf.read(name)
        outfile = os.path.join(tempdir, name)
        if name.endswith('.shp'):
            shape_path = outfile
        f = open(outfile, 'w')
        f.write(data)
        f.close()

    if shape_path is None:
        log.warn("No shapefile, cleaning up")
        # Clean up after ourselves.
        for file in os.listdir(tempdir):
            os.unlink(os.path.join(tempdir, file))
        os.rmdir(tempdir)
        raise ValueError("No shapefile found in zip")

    return shape_path
