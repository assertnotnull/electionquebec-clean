import os
from django.core.management.base import BaseCommand
from pykml import parser
from circonscriptions.models import Circonscription

class Command(BaseCommand):
    help = 'Import kml files to a specific type'
    option_list = BaseCommand.option_list

    def get_version(self):
        return '0.1'

    def parse(self, file):
        kmlfile = parser.parse(file)
        root = kmlfile.getroot()
        data = {}
        placemark = root.Document.Folder.Placemark
        for element in placemark.ExtendedData.SchemaData.SimpleData:
            data[element.get('name')] = element.text
        for element in placemark.getchildren():
            if element.tag.rfind('Polygon') is not -1:
                for element in placemark.Polygon.outerBoundaryIs.LinearRing.coordinates:
                    data['coordinates'] = element.text
            elif element.tag.rfind('MultiGeometry') is not -1:
                polygones = []
                for polygon in placemark.MultiGeometry.Polygon:
                    polygones.append(polygon.outerBoundaryIs.LinearRing.coordinates.text)
                data['coordinates'] = ";".join(polygones)
        circonscirption = Circonscription.objects.get(id_cep = data['id_cep'])
        circonscirption.coordinates = data['coordinates']
        circonscirption.save()

    def handle(self, *args, **options):
        path = 'data/kml/'
        if len(args) is 0:
            for item in os.listdir(path):
                if os.path.isfile(os.path.join(path, item)):
                    with open(path + item) as file:
                        print item
                        self.parse(file)
        else:
            file = open(path + args[0].upper() + '.kml')
            self.parse(file)