from settings import *

DEBUG = False

ADMINS = (
#    ('Patrice Gauthier', '')
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'electionquebec',                      # Or path to database file if using sqlite3.
        'USER': '-',                      # Not used with sqlite3.
        'PASSWORD': '-',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}
