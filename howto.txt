How to setup for mysql:

sudo apt-get install mysql-server mysql-client libmysqlclient15-dev python-dev

libmysqlclient15-dev contains mysql_config tool needed by MySQL-python (alias MySQLdb)
python-dev required for python package MySQL-python

--
libraries needed for pykml:
sudo apt-get install libxml2-dev libxslt1-dev

--
install geos lib:

wget http://download.osgeo.org/geos/geos-3.3.0.tar.bz2
tar xjf geos-3.3.0.tar.bz2
sudo apt-get install g++
cd geos-3.3.0
./configure
make
sudo make install
NOTE : in case of failing with (libgeos_c.so.1: cannot open shared object file) , run : ldconfig

--
install lib apache2 wsgi:
sudo apt-get install libapache2-mod-wsgi
